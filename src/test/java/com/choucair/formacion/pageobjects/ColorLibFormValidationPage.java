package com.choucair.formacion.pageobjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class ColorLibFormValidationPage extends PageObject {
	
	//Required 
	@FindBy(xpath="//*[@id=\'req\']")
	public WebElementFacade txtRequired;
	//Select 
	@FindBy(xpath="//*[@id=\'sport\']")
	public WebElementFacade cmbSport1;
	//MultipleS1 
	@FindBy(xpath="//*[@id=\'sport2\']")
	public WebElementFacade cmbSport2;
	//Url	 
	@FindBy(xpath="//*[@id=\'url1\']")
	public WebElementFacade txtUrl;
	//E-mail
	@FindBy(xpath="//*[@id=\'email1\']")
	public WebElementFacade txtEmail;
	//Password1	
	@FindBy(xpath="//*[@id=\'pass1\']")
	public WebElementFacade txtPassword1;
	//Password2	
	@FindBy(xpath="//*[@id=\'pass2\']")
	public WebElementFacade txtPassword2;
	//MinSize
	@FindBy(xpath="//*[@id=\'minsize1\']")
	public WebElementFacade txtMinSize;
	//MaxSize
	@FindBy(xpath="//*[@id=\'maxsize1\']")
	public WebElementFacade txtMaxSize;
	//Number
	@FindBy(xpath="//*[@id=\'number2\']")
	public WebElementFacade txtNumber;
	//IP	
	@FindBy(xpath="//*[@id=\'ip\']")
	public WebElementFacade txtIp;
	//Date	
	@FindBy(xpath="//*[@id=\'date3\']")
	public WebElementFacade txtDate;
	//DateEarlier  
	@FindBy(xpath="//*[@id=\'past\']")
	public WebElementFacade txtDateEarlier;
	
	public void Required(String datoPrueba) {
		txtRequired.click();
		txtRequired.clear();
		txtRequired.sendKeys(datoPrueba);
	}
	
	public void Select_Sport(String datoPrueba) {
		cmbSport1.click();
		cmbSport1.selectByVisibleText(datoPrueba);
	}
	
	public void Multiple_Select (String datoPrueba) {
		cmbSport2.click();
		cmbSport2.selectByVisibleText(datoPrueba);
	}
	

	public void url (String datoPrueba) {
		txtUrl.click();
		txtUrl.clear();
		txtUrl.sendKeys(datoPrueba);
		
	}
	public void Email (String datoPrueba) {
		txtEmail.click();
		txtEmail.clear();
		txtEmail.sendKeys(datoPrueba);
		
	}
	public void Password (String datoPrueba) {
		txtPassword1.click();
		txtPassword1.clear();
		txtPassword1.sendKeys(datoPrueba);
	
	}
	public void Confirm_Password(String datoPrueba) {
		txtPassword2.click();
		txtPassword2.clear();
		txtPassword2.sendKeys(datoPrueba);
		
	}
	public void Minimum_field_size(String datoPrueba) {
		txtMinSize.click();
		txtMinSize.clear();
		txtMinSize.sendKeys(datoPrueba);
		
	}
	public void Maximum_field_size (String datoPrueba) {
		txtMaxSize.click();
		txtMaxSize.clear();
		txtMaxSize.sendKeys(datoPrueba);
	
	}
	public void Number (String datoPrueba) {
		txtNumber.click();
		txtNumber.clear();
		txtNumber.sendKeys(datoPrueba);
	}
	public void Ip (String datoPrueba) {
		txtIp.click();
		txtIp.clear();
		txtIp.sendKeys(datoPrueba);
		
	}
	public void Date (String datoPrueba) {
	
		txtDate.click();
		txtDate.clear();
		txtDate.sendKeys(datoPrueba);
	}
	public void Date_Earlier (String datoPrueba) {
		
		txtDateEarlier.clear();
		txtDateEarlier.click();
		txtDateEarlier.sendKeys(datoPrueba);
	}
	
	//Botón Validate
	@FindBy (xpath = "//*[@id=\'popup-validation\']/div[14]/input")
	public WebElementFacade btnValidate;
	
	public void validate() {
		btnValidate.click();
	}
	

		//Globo Informativo
		@FindBy(xpath="(//DIV[@class='formErrorContent'])[1]")
		public WebElementFacade globoInformativo;
		
		public void form_sin_errores() {
			assertThat(globoInformativo.isCurrentlyVisible(), is (false));
		}
		public void form_con_errores() {
			assertThat(globoInformativo.isCurrentlyVisible(), is (true));
		}

}
