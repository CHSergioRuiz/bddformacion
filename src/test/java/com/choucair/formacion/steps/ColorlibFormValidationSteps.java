package com.choucair.formacion.steps;

import java.util.List;

import com.choucair.formacion.pageobjects.ColorLibFormValidationPage;

import net.thucydides.core.annotations.Step;





public class ColorlibFormValidationSteps {

	ColorLibFormValidationPage 	colorLibFormValidationPage;
	
	@Step
	public void diligenciar_popup_datos_tabla(List<List<String>> data, int id) {
		colorLibFormValidationPage.Required(data.get(id).get(0).trim());
		colorLibFormValidationPage.Select_Sport(data.get(id).get(1).trim());
		colorLibFormValidationPage.Multiple_Select(data.get(id).get(2).trim());
		colorLibFormValidationPage.Multiple_Select(data.get(id).get(3).trim());
		colorLibFormValidationPage.url(data.get(id).get(4).trim());
		colorLibFormValidationPage.Email(data.get(id).get(5).trim());
		colorLibFormValidationPage.Password(data.get(id).get(6).trim());
		colorLibFormValidationPage.Confirm_Password(data.get(id).get(7).trim());
		colorLibFormValidationPage.Minimum_field_size(data.get(id).get(8).trim());
		colorLibFormValidationPage.Maximum_field_size (data.get(id).get(9).trim());
		colorLibFormValidationPage.Number(data.get(id).get(10).trim());
		colorLibFormValidationPage.Ip(data.get(id).get(11).trim());
		colorLibFormValidationPage.Date(data.get(id).get(12).trim());
		colorLibFormValidationPage.Date_Earlier(data.get(id).get(13).trim());
	}
	
	@Step
	public void verificar_ingreso_datos_formulario_exitoso() {
		colorLibFormValidationPage.form_sin_errores();
		
	}
	
	@Step
	public void verificar_ingreso_datos_formulario_con_errores() {
		colorLibFormValidationPage.form_con_errores();
		
	}
}
